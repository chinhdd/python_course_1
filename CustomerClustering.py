import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

customer_df = pd.read_csv('customers.csv')

print(customer_df.head())
print(customer_df.describe())

# print(customer_df['Channel'].values)
# print(np.any(customer_df['Channel'].values))
# print(np.all(customer_df['Channel'].values))
# print(customer_df[0])

# remove two columns Channel and Region and then cluster base on other columns
channel_column = customer_df['Channel']
region_column = customer_df['Region']

customer_product_df = customer_df.drop(['Channel', 'Region'],axis= 1)
print(customer_product_df)

# draw histogram of each column
columns = ['Fresh','Milk','Grocery','Frozen','Detergents_Paper','Delicatessen']
for column in columns:
    series_column = customer_product_df[column]
    plt.hist(series_column.values, bins=50, density=1, facecolor='green', alpha=0.75)
    plt.xlabel(column)
    plt.ylabel('Frequency')
    plt.title('Distribution of ' + column +' product')
    plt.grid(True)
    plt.show()

    # calculate the difference between two continuous element
    value_column = series_column.values
    value_column.sort()
    diff_value = np.diff(value_column)
    plt.hist(diff_value, bins=50, density=1, facecolor='blue', alpha=0.75)
    plt.xlabel('Difference')
    plt.ylabel('Frequency')
    plt.title('Difference between elements in ' + column)
    plt.grid(True)
    plt.show()

    # remove zero difference
    num_zero = np.sum(diff_value == 0)
    print(num_zero)

