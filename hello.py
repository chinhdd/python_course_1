import sys
from typing import List, Any
from prompt_toolkit import formatted_text

print('Hello world')
i: int = 5

print('something')
print(13 / 3)
print(13 // 3)
print('Hello', i)

if i % 2 == 0:
    print('{} is even'.format(i))
else:
    print('{} is odd'.format(i))
    print("end if")
print('Hello' * i)

g: List[Any] = sys.argv
print(type(g))
print(sys.argv)

h = sys.path
print(type(h))
assert isinstance(h, object)
print(h)

print(__name__)

print(dir(sys))

print(dir())

nameNumber = {
    'one': 1,
    'two': 2,
    'three': 3
}

print(type(nameNumber))
print(type(nameNumber.items()))
print(len(nameNumber))
for (name, number) in nameNumber.items():
    print(name, 'has number', number)
    print(type((name, number)))


class Person:
    pass


p = Person()
print(p)
